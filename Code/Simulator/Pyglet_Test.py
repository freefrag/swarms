import pyglet
from pyglet.gl import *
from pyglet.window import key
from Simulator import Simulator

window = pyglet.window.Window(resizable=True)
simulator = Simulator(500)

def update(dt):
  simulator.step(dt)
pyglet.clock.schedule(update)

def setup():
    # One-time GL setup
    glClearColor(0, 0, 0, 0)
    glColor3f(1, 1, 1)
    glEnable(GL_DEPTH_TEST)
    glEnable(GL_CULL_FACE)

x = 0
y = 0
z = -5

ry = rz = 0
@window.event
def on_key_press(symbol, modifiers):
  global x, y, z, ry, rz
  if symbol == key.P:
    z += 0.1
  if symbol == key.O:
    z -= 0.1
  if symbol == key.W:
    y += 0.1
  if symbol == key.S:
    y -= 0.1
  if symbol == key.A:
    x -= 0.1
  if symbol == key.D:
    x += 0.1
  if symbol == key.LEFT:
    rz += 3
  if symbol == key.RIGHT:
    rz -= 3
    

@window.event
def on_draw():
  global x, y, z, ry, rz
  # Clear buffers
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
  glClearDepth(1.0)
  glEnable(GL_DEPTH_TEST)
  glDepthFunc(GL_LEQUAL)

  glMatrixMode(GL_MODELVIEW)
  glLoadIdentity()

  glTranslatef(x, y, z)
  glRotatef(rz, 0, 1, 0)
  # Draw some stuff
  glBegin(GL_POINTS)
  for p in simulator.particles:
    intensity = p.position[2] * 0.7 + 0.3
    glColor3f(intensity, intensity, intensity)
    glVertex3f(-3 * p.position[0], 2 * p.position[1], p.position[2])
  glEnd()

@window.event
def on_resize(width, height):
    # Override the default on_resize handler to create a 3D projection
    glViewport(0, 0, width, height)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    gluPerspective(60., width / float(height), .1, 1000.)
    glMatrixMode(GL_MODELVIEW)
    return pyglet.event.EVENT_HANDLED

setup()
pyglet.app.run()
