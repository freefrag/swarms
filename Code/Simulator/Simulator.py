import copy
import numpy as np
import numpy.random as random
from quaternion import Quaternion
from scipy.spatial import cKDTree
from scipy.spatial import KDTree
from pyflann import *

class Simulator:
  INTERACTION_NUMBER = 5    # Number of Nearest-Neighbours to be interacted with
  INTERACTION_RADIUS = 0.01    # Distance within which particles interact
  
  def __init__(self, num_particles = 80000):
    self.num_particles = num_particles
    self.particles = [Particle() for x in xrange(num_particles)]
    
  def step(self, delta_time):
    positions = np.array(map(lambda p: p.position, self.particles))
    old_particles = copy.deepcopy(self.particles)

    ##tree = cKDTree(positions)

    ##distances, neighbors = tree.query(positions, eps = 0.5, k=self.INTERACTION_NUMBER + 1, distance_upper_bound = self.INTERACTION_RADIUS)

    flann = FLANN()
    neighbors, distances = flann.nn(positions, positions, self.INTERACTION_NUMBER, algorithm="kmeans", branching=32, iterations=7, checks = 16)
    neighbor_number = map(lambda n: len(set(n)), neighbors)
    for i, neighbor_indices in enumerate(neighbors):
      #Convert from indices to particle objects
      neighbor_particles = map(lambda n: old_particles[n], neighbor_indices)
      self.particles[i].update_position(neighbor_particles[1:], delta_time)



class Particle:
  def __init__(self, position = None, attitude = None, speed = 0.01):
    if position is None:
      position = random.random(3)
    if attitude is None:
      attitude = Quaternion.random_rotation()

    self.speed = np.array((speed, 0, 0))
    self.position = position
    self.attitude = attitude

  def update_position(self, neighbors, delta_time):
   def normalize(v):
     n = np.sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2])
     return (v[0] / n, v[1] / n, v[2] /n)

   def diff(v1, v2):
    d1 = v1[0] - v2[0]
    d2 = v1[1] - v2[1]
    d3 = v1[2] - v2[2]
    return (d1, d2, d3)

   distances = map(lambda n: diff(n.position, self.position), neighbors)
   attitudes = map(lambda n: n.attitude, neighbors)

   for i, dist in enumerate(distances):
     d = dist[0] * dist[0] + dist[1] * dist[1] + dist[2] * dist[2]
     if d < 0.00001:
       attitudes[i] = attitudes[i].inverse() * 5


   average_attitude = Quaternion.average(attitudes)
   average_attitude = Quaternion.average([average_attitude, Quaternion.random_rotation() * 0.1])

   self.attitude = average_attitude
   if any(map(lambda x: x < 0 or x > 1, self.position)):
     self.attitude = Quaternion.from_vector_vector(self.speed,np.array((0.5,0.5,0.5)) -self.position)
    # print 'pos: ' + str(self.position)
    # print 'att: ' + str(self.attitude.rotate(self.speed))

   self.position += self.attitude.rotate(self.speed) * delta_time

     

   
