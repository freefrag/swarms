import numpy as np
import numbers
import numpy.random as random

class Quaternion:

  def __init__(self, q0 = 0, q1 = 0, q2 = 0, q3 = 0): 
    self.q0 = float(q0)
    self.q1 = float(q1)
    self.q2 = float(q2)
    self.q3 = float(q3)

  # STATIC METHODS

  # Returns a random rotation quaternion, sampled uniformly on SO(3)
  # Due to: http://planning.cs.uiuc.edu/node198.html
  @staticmethod
  def average(qs):
    total = Quaternion(0, 0, 0, 0)
    for i, q in enumerate(qs):
      if qs[0].dot(q) < 0:
        q = -q
      total = total + q

    return (total).normalize()

  @staticmethod
  def from_vector_angle(vector, angle):
    sin_angle = np.sin(angle / 2.0)
    cos_angle = np.cos(angle / 2.0)
    q0 = cos_angle
    q1 = vector[0] * sin_angle
    q2 = vector[1] * sin_angle
    q3 = vector[2] * sin_angle
    return Quaternion(q0, q1, q2, q3).normalize()

  @staticmethod
  def from_vector_vector(v1, v2):
    norms = np.linalg.norm(v1) * np.linalg.norm(v2)
    cos = np.dot(v1, v2) / norms 
    half_cos = np.sqrt(0.5 * (1 + cos))
    cross = np.cross(v1, v2) / (norms * 2 * half_cos)
    return Quaternion(half_cos, cross[0], cross[1], cross[2])

  @staticmethod
  def random_rotation():
    i, j, k = random.random(), random.random(), random.random()
    q0 = np.sqrt(1 - i) * np.sin(2 * np.pi * i) 
    q1 = np.sqrt(1 - i) * np.cos(2 * np.pi * j) 
    q2 = np.sqrt(i) * np.sin(2 * np.pi * k) 
    q3 = np.sqrt(i) * np.cos(2 * np.pi * k) 
    return Quaternion(q0, q1, q2, q3).normalize()

  # MAGIC METHODS  
  TYPE_ERROR_MESSAGE = "Unsupported operand type for {0}: '{1}', and '{2}'"

  # Absolute Value
  def __abs__(self):
    return self.norm()

  # Addition
  def __add__(self, other):
    if isinstance(other, self.__class__):
      q0 = self.q0 + other.q0
      q1 = self.q1 + other.q1
      q2 = self.q2 + other.q2
      q3 = self.q3 + other.q3
      return Quaternion(q0, q1, q2, q3)
    if isinstance(other, numbers.Number) and not isinstance(other, complex):  
      return Quaternion(q0 + other, q1, q2, q3)
    else:
      raise TypeError(TYPE_ERROR_MESSAGE.format('+', self.__class__, type(other))) 

  # Division
  def __div__(self, other):
    if isinstance(other, self.__class__):
      return self * other.inverse()
    if isinstance(other, numbers.Number) and not isinstance(other, complex):  
      q0 = self.q0 / other
      q1 = self.q1 / other
      q2 = self.q2 / other
      q3 = self.q3 / other
      return Quaternion(q0, q1, q2, q3)
    else:
      raise TypeError(TYPE_ERROR_MESSAGE.format('+', self.__class__, type(other))) 
   

  # Multiplication
  def __mul__(self, other):
    if isinstance(other, self.__class__):
      a = self
      b = other
      q0 = a.q0 * b.q0 - a.q1 * b.q1 - a.q2 * b.q2 - a.q3 * b.q3
      q1 = a.q0 * b.q1 + a.q1 * b.q0 + a.q2 * b.q3 - a.q3 * b.q2
      q2 = a.q0 * b.q2 - a.q1 * b.q3 + a.q2 * b.q0 + a.q3 * b.q1
      q3 = a.q0 * b.q3 + a.q1 * b.q2 - a.q2 * b.q1 + a.q3 * b.q0
      return Quaternion(q0, q1, q2, q3)
    if isinstance(other, numbers.Number) and not isinstance(other, complex):  
      return Quaternion(self.q0 * other, self.q1 * other, self.q2 * other, self.q3 * other)
    else:
      raise TypeError(TYPE_ERROR_MESSAGE.format('*', self.__class__, type(other)))

  # Negation
  def __neg__(self):
    q0 = -self.q0
    q1 = -self.q1
    q2 = -self.q2
    q3 = -self.q3
    return Quaternion(q0, q1, q2, q3)

  # To String
  def __str__(self):
    format_parts = []
    if self.q0 != 0:
      format_parts.append("{0}")
    if self.q1 != 0:
      format_parts.append("{1}i")
    if self.q2 != 0:
      format_parts.append("{2}j")
    if self.q3 != 0:
      format_parts.append("{3}k")

    format_string = '(' + ' + '.join(format_parts) + ')'

    return format_string.format(self.q0, self.q1, self.q2, self.q3)

  # Returns the conjugate of the quaternion
  def conjugate(self):
    return Quaternion(self.q0, -self.q1, -self.q2, -self.q3)
  
  # Returns the dot product with the given quaternion
  def dot(self, other):
    return self.q0 * other.q0 + self.q1 * other.q1 + self.q2 * other.q2 + self.q3 * other.q3

  # Returns the inverse of the quaternion
  def inverse(self):
    return self.conjugate() / self.norm_squared()

  # Returns the norm of the quaternion
  def norm(self):
    return np.sqrt(self.norm_squared())

  # Returns the squared norm of the quaternion
  def norm_squared(self):
    return self.dot(self)

  # Returns the quaternion normalized
  def normalize(self):
    return self / self.norm()
  
  # Rotates the given vector by the quaternion
  def rotate(self, vector):
    (v1, v2, v3) = vector
    rot = self * Quaternion(0, v1, v2, v3) * self.inverse()
    return np.array((rot.q1, rot.q2, rot.q3))
    

# TESTS
def inverse_test(a):
 print "inverse of {0} is {1}, {0} * {1} = {2}".format(a, a.inverse(), a * a.inverse())

def mult_test(a, b):
  print "{0} * {1} = {2}".format(a, b, a * b)

def norm_test(a):
  print "|{0}| = {1}".format(a, a.norm())

def normalize_test(a):
  normal = a.normalize()
  print "{0} (norm {1:.2f}) normalized is {2} norm({3})".format(a, a.norm(), normal, normal.norm())

if __name__ == "__main__":
  i = Quaternion(0, 1, 0, 0)
  j = Quaternion(0, 0, 1, 0)
  k = Quaternion(0, 0, 0, 1)

  print "TEST MULTIPLICATIVE IDENTITIES"
  mult_test(i, j)
  mult_test(i, k)
  mult_test(j, i)
  mult_test(j, k)
  mult_test(k, i)
  mult_test(k, j)
  mult_test(i, i)
  mult_test(j, j)
  mult_test(k, k)

  print "\nTEST INVERSE"
  inverse_test(i + j + k)
  inverse_test(i)
  inverse_test(Quaternion(1, 0, 14, 0))
  inverse_test(Quaternion(1, -2, 1.2, 4))

  print "\nTEST NORM"
  norm_test(Quaternion(1, 0, 0, 0))
  norm_test(i)
  norm_test(i + j)
  norm_test(i + k)
  norm_test(i + j + k)
  norm_test(Quaternion(1, 2.1, -6, 3.4))

  print "\nTEST NORMALIZATION"
  normalize_test(i)
  normalize_test(j+k)
  normalize_test(i+j+k)
  normalize_test(Quaternion(1, 2, 3 , 4))
