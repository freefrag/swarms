#include <AntTweakBar.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <OpenGL/OpenGL.h>
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>
#include "experiment.h"
#include "simulator.h"
#include "statistics.h"
#include "update_rules.h"

Simulator sim;
Experiment experiment;

void update() {
  glutPostRedisplay();
}

/* GLOBALS */
float x_cam = 0;
float y_cam = 0;
float z_cam = 0;
float y_cam_rot = 0;
float particle_scale = 0.005;
int pause = 1;

void handle_keyboard(unsigned char key, int x, int y){
  if( TwEventKeyboardGLUT(key, x, y) ){
    // Event has been handled by AntTweakbar
    return;
  }
  if(key == 'w'){
    z_cam += 0.06;
  }
  if(key == 's'){
    z_cam -= 0.06;
  }
  if(key == 'a'){
    y_cam_rot -= 1.5; 
  }
  if(key == 'd'){
    y_cam_rot += 1.5;
  }
  if(key == '+' || key == '='){
    particle_scale *= 1.3f;
  }
  if(key == '_' || key == '-'){
    particle_scale /= 1.3f;
  }
  if(key == ' '){
    pause = !pause;
  }
}

void draw_frame_number(int frame){
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0.0, 100, 0.0, 100);
  glColor3f(1.0,1.0,1.0);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glRasterPos2i(10, 17);

  char str[30];
  sprintf(str, "frame: %d", frame++);
  void * font = GLUT_BITMAP_9_BY_15;

  int i = 0;
  while(str[i] != 0){
    glutBitmapCharacter(font, str[i]);
    i++;
  }

  double variance = 1;
  //variance = average_neighbors(&sim);
  //variance = estimate_k(&sim);
  //variance = attitude_k_means(&sim, 8);
  variance = calculate_variance(&sim);
  sprintf(str, "PCA order: %.3f", variance);

  i = 0;
  glRasterPos2i(10, 11);
  while(str[i] != 0){
    glutBitmapCharacter(font, str[i]);
    i++;
  }

  double order = 1;
  //order = count_distance_clusters(&sim);
  //order = average_neighbors(&sim);
  //sprintf(str, "distance clusters: %.3f", order);

  i = 0;
  glRasterPos2i(10, 5);
  while(str[i] != 0){
    glutBitmapCharacter(font, str[i]);
    i++;
  }
}

void draw_particle(){
  glPushMatrix();
  glScalef(particle_scale, particle_scale, particle_scale);
  glRotatef(270, 0, 0, 1);
  glBegin(GL_TRIANGLES);
    //left side triangle
    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glVertex3f( -1.0f, -1.0f, 0.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);
    glVertex3f( 0.0f,  -1.0f, -1.0f);

    //front triangle
    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);
    glVertex3f( -1.0f, -1.0f, 0.0f);
    glVertex3f( 1.0f,  -1.0f, 0.0f);

    //right side triangle
    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    glVertex3f( 1.0f,  -1.0f, 0.0f);
    glVertex3f(0.0f, 1.0f, 0.0f);
    glVertex3f( 0.0f,  -1.0f, -1.0f);

    //bottom triangle
    glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
    glVertex3f( -1.0f, -1.0f, 0.0f);
    glVertex3f( 1.0f,  -1.0f, 0.0f);
    glVertex3f( 0.0f,  -1.0f, -1.0f);
  glEnd();
  glPopMatrix();
}

void draw_universe(){
  glPushMatrix();
  glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
  glTranslatef(-0.5f, -0.5f, -0.5f);
  glBegin(GL_QUADS);
    glColor3f(0.82, 0.41, 0.12);//this the color with which complete cube is drawn. 
    glVertex3f(0,0 ,0 );
    glVertex3f(1, 0, 0);
    glVertex3f(1, 1, 0);
    glVertex3f(0, 1, 0);

    //face in yz plane
    glColor3f(1, 0, 0);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 1);
    glVertex3f(0, 1, 1);
    glVertex3f(0, 1, 0);

    //face in zx plance
    glColor3f(0, 1, 0);
    glVertex3f(0, 0, 0  );
    glVertex3f(0, 0, 1);
    glVertex3f(1, 0, 1);
    glVertex3f(1, 0, 0);

    //|| to xy plane.
    glColor3f(0, 0, 1);
    glVertex3f(0, 0, 1);
    glVertex3f(1, 0, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(0, 1, 1);

    //|| to yz plane
    glColor3f(0.73, 0.18, 0.18);
    glVertex3f(0,0 ,1 );
    glVertex3f(1, 0, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(0, 1, 1);

    //|| to zx plane
    glColor3f(0.18, 0, 0.82);
    glVertex3f(0, 1, 0  );
    glVertex3f(0, 1, 1);
    glVertex3f(1, 1, 1);
    glVertex3f(1, 1, 0);
  glEnd();
  glPopMatrix();
}

void reshape(int width, int height){
  glViewport(0, 0, width, height);
  TwWindowSize(width, height);
}

void display(){
  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(45.0f, 1.0f, 0.0333f, 300.0f);
  gluLookAt(0,0,-2.0f, 0,0,1, 0,1,0);
  glMatrixMode(GL_MODELVIEW);
  glTranslatef(0, 0, z_cam);
  glRotatef(y_cam_rot, 0.0f, 1.0f, 0.0f);

  if(!pause){
    step_simulator(&sim);
    record_data(&sim, &experiment);
  }
  int remaining = sim.num_particles;
  while(remaining > 0){
    int i = rand_range(0, remaining);
    glPushMatrix();
    double angle;
    double x_rot;
    double y_rot;
    double z_rot;
    
    
    float x = (float)sim.particles[i]->pos.x - 0.5;
    float y = (float)sim.particles[i]->pos.y - 0.5;
    float z = (float)sim.particles[i]->pos.z - 0.5;
    glTranslatef(x, y, z);

    toAxisAngle(&sim.particles[i]->rot, &angle, &x_rot, &y_rot, &z_rot);
    angle = angle * 180 / M_PI;
    glRotatef(angle, x_rot, y_rot, z_rot);
    draw_particle();
    glPopMatrix();

    remaining--;
    Particle* last = sim.particles[remaining];
    sim.particles[remaining] = sim.particles[i];
    sim.particles[i] = last;
  }
  draw_universe();
  draw_frame_number(sim.frame);
  TwDraw();
  glFlush();
  glutSwapBuffers();
}

int cur_rule = 0;
void TW_CALL set_update_rule(void* const value, void *clientData){
  cur_rule = *(int*)value;
  sim.rule = &all_rules.rules[cur_rule];
}

void TW_CALL get_update_rule(void* const value, void *clientData){
  *(int*)value = cur_rule;
}

void twInit(int width, int height){
  TwInit(TW_OPENGL, NULL);
  TwWindowSize(width, height);
  TwGLUTModifiersFunc(glutGetModifiers);  
  TwBar *bar;
  bar = TwNewBar("TweakBar");
  TwDefine(" TweakBar size='250 100' color='96 216 224' "); // change default tweak bar size and color

  // Set GLUT event callbacks
  // - Directly redirect GLUT mouse button events to AntTweakBar
  glutMouseFunc((GLUTmousebuttonfun)TwEventMouseButtonGLUT);
  // - Directly redirect GLUT mouse motion events to AntTweakBar
  glutMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);
  // - Directly redirect GLUT mouse "passive" motion events to AntTweakBar (same as MouseMotion)
  glutPassiveMotionFunc((GLUTmousemotionfun)TwEventMouseMotionGLUT);
  // - Directly redirect GLUT key events to AntTweakBar
  // - Directly redirect GLUT special key events to AntTweakBar
  glutSpecialFunc((GLUTspecialfun)TwEventSpecialGLUT);
  // - Send 'glutGetModifers' function pointer to AntTweakBar;
  //   required because the GLUT key event functions do not report key modifiers states.
  TwGLUTModifiersFunc(glutGetModifiers);

  //INITIALIZE THE TWEAK BAR
  TwAddVarRW(bar, "Noise", TW_TYPE_DOUBLE, &sim.noise, "min=0 max=0.5 step=0.002");
  TwAddVarRW(bar, "Radius (Interaction)",TW_TYPE_DOUBLE, &sim.interaction_radius,
             "min=0 max=1 step=0.01");

  //CREATE THE RULES SELECTOR
  TwEnumVal attitudeEV[all_rules.count];
  for(int i = 0; i < all_rules.count; i++){
    attitudeEV[i].Value = i;
    attitudeEV[i].Label = all_rules.rules[i].name;
  }
  TwType attitudeEnum = TwDefineEnum("Attitude Rule", attitudeEV, all_rules.count);
  TwAddVarCB(bar, "Rule", attitudeEnum, &set_update_rule, &get_update_rule, NULL , "");
}

int main(int argc, char** argv){
  srand(time(NULL));
  init_experiment(&experiment);
  init_simulator(&sim, 5000,0.0, 0.05, &all_rules.rules[2]);
  glutInit(&argc, argv);                 
  glutCreateWindow("Swarm Simulation");
  glutInitWindowSize(3000, 3000);   
  glutInitWindowPosition(50, 50); 
  glutDisplayFunc(display); 
  glutReshapeFunc(reshape);
  glutKeyboardFunc(handle_keyboard);
  glutIdleFunc(update);
  glEnable(GL_DEPTH_TEST);
  twInit(800, 800);
  glutMainLoop();
  return 0;
}
