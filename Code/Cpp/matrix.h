#ifndef MATRIX_H
#define MATRIX_H

typedef struct {
  int x_dimension, y_dimension;
  double* data;
} Matrix;

/* Sets the value in the matrix at the given index */
void m_set(Matrix*, int, int, double);
/* Returns the value of the matrix at the specified index */
double m_get(Matrix*, int, int);
/* Add the first two matrices and stores the result in the destintion. 
 * Only works if all three matrices are of the same dimension */
void m_add(Matrix*, Matrix*, Matrix*);
/* Initializes a Matrix with the given dimensions and allocated the necessary memory space for it
 * Remember to destroy the matrix with destroy_matrix */
void init_matrix(Matrix*, int, int);
/* Cleans the memory used by this matrix and deallocates it */
void destroy_matrix(Matrix*);

#endif

