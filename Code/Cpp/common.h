#ifndef COMMON_H
#define COMMON_H
#include <stdlib.h>

typedef struct {
  double x, y, z;
} Vector3;

typedef struct {
  double x, y, z, w;
} Vector4;


/* Returns the first number (positive) modulo the second */
int pos_mod(int, int);
double pos_fmod(double, double);
/* Reallocates memory and sets it all to zero */
void* realloc_zero(void*, size_t old_size, size_t new_size);
/* Returns a 'uniformly sampled' integer between Low (inclusive) and High (exclusive) */
int rand_range(int, int);
/* Returns an approximately uniformly random double in the specified range */
double rand_double(void);
/* Stores an approximately uniformly random position inside the unit box in the given vector */ 
void rand_position(Vector3*);
/* Calculates the cross product of the first two vector and stores result in output vector */
void vec3_cross(Vector3*, Vector3*, Vector3*);
/* Returns the dot product of the two input vectors */
double vec3_dot(Vector3*, Vector3*);
/* Normalizes the input vector and stores the result in the output vector */
void vec3_normalize(Vector3*, Vector3*);
/* Adds the first two vectors together and stores result in third vector */
void vec3_add(Vector3*, Vector3*, Vector3*);
/* Returns the square of the distance between the two vectors */
double vec3_dist(Vector3*, Vector3*);



#endif
