#ifndef QUATERNION_H
#define QUATERNION_H
#include "common.h"
#include "matrix.h"

typedef struct {
  double q0;
  double q1;
  double q2;
  double q3;
} Quaternion;

void multiply(Quaternion*, Quaternion*, Quaternion*);
void toAxisAngle(Quaternion*, double*, double*, double*, double*);
void toEuler(Quaternion*, double*, double*, double*);
double dist(Quaternion*, Quaternion*); 
double dot(Quaternion*, Quaternion*);
void negate(Quaternion*);
void conjuage(Quaternion*);
void inverse(Quaternion*);
void normalize(Quaternion*);
void random_rotation(Quaternion*);
void outer_product(Quaternion*, Quaternion*, Matrix*);
void rotate(Quaternion*, Vector3*);
void from_vector_vector(Vector3*, Vector3*, Quaternion*);

double norm_squared(Quaternion*);
double norm(Quaternion*);


#endif
