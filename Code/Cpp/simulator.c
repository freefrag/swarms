#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "common.h"
#include "particle_man.h"
#include "simulator.h"
#include "simulator_common.h"
#include "statistics.h"
#include "update_rules.h"

void init_simulator(Simulator *simulator, size_t num_particles,
                    double noise, double i_radius, Update_Rule *rule)
{
  if (rule == NULL){
    rule = &all_rules.rules[0];
  }

  init_grid(&simulator->grid, num_particles);
  simulator->frame = 0;
  simulator->num_particles = num_particles;
  simulator->noise = noise;
  simulator->interaction_radius = i_radius;
  simulator->rule = rule;
  simulator->particles = malloc(num_particles * sizeof(Particle*));
  simulator->particle_storage = malloc(num_particles * sizeof(Particle));

  int i;
  for (i = 0; i<num_particles; i++){
    Particle* p = simulator->particle_storage + i;
    p->id = i;
    p->pos.x = rand_double();
    p->pos.y = rand_double();
    p->pos.z = rand_double();
    
    random_rotation(&p->rot);
    insert_particle(&simulator->grid, p);
    simulator->particles[i] = p;
  }
}

void update_position(Particle* particle, double rand_scale){
  Quaternion random;
  random_rotation(&random);
  if(dot(&particle->rot, &random) < 0){
    negate(&random);
  }
  particle->rot.q0 += random.q0 * rand_scale;
  particle->rot.q1 += random.q1 * rand_scale;
  particle->rot.q2 += random.q2 * rand_scale;
  particle->rot.q3 += random.q3 * rand_scale;
  normalize(&particle->rot);

  Vector3 speed;
  speed.x = 0.005;
  speed.y = 0;
  speed.z = 0;

  rotate(&particle->rot, &speed);

  particle->pos.x += speed.x;
  particle->pos.y += speed.y;
  particle->pos.z += speed.z;

  particle->pos.x = pos_fmod(particle->pos.x, 1);
  particle->pos.y = pos_fmod(particle->pos.y, 1);
  particle->pos.z = pos_fmod(particle->pos.z, 1);
}

void step_simulator(Simulator* sim){
  int i;
  for (i=0; i < sim->num_particles; i++){
    Particle* particle_buffer[sim->rule->neighbors];
    remove_particle(&sim->grid, sim->particles[i]);

    int j;
    for(j = 0; j<sim->rule->neighbors; j++){
      particle_buffer[j] = (Particle*)j;
    }

    int n = find_neighbors(
      &sim->grid,
      &sim->particles[i]->pos,
      sim->interaction_radius,
      sim->rule->neighbors,
      particle_buffer);

    sim->rule->update_attitude(sim->particles[i], particle_buffer, n);
    update_position(sim->particles[i], sim->noise);
    insert_particle(&sim->grid, sim->particles[i]);
  }
  sim->frame++;
}
