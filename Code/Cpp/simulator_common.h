#ifndef SIMULATOR_COMMON_H
#define SIMULATOR_COMMON_H
#include <stdlib.h>
#include "quaternion.h"
#include "common.h"

typedef struct {
  int id;
  Vector3 pos;
  Quaternion rot;
  int cluster;
} Particle;

#endif
