#ifndef STATISTICS_H
#define STATISTICS_H

double calculate_variance(Simulator*);
double calculate_velocity_order(Simulator*);
int count_distance_clusters(Simulator*);
double attitude_k_means(Simulator*, int);
double average_neighbors(Simulator*);
int estimate_k(Simulator*);

#endif
