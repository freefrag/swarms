#include <math.h>
#include <stdio.h>
#include "common.h"
#include "quaternion.h"
#include "simulator.h"

static double calculate_variance_raw(Particle**, int);

double calculate_variance(Simulator* sim){
  return calculate_variance_raw(sim->particles, sim->num_particles);
}

double calculate_variance_raw(Particle** particles, int num_particles){
  if(num_particles < 2){
    return 0;
  }

  Matrix average_product;
  Matrix current_product;

  init_matrix(&average_product, 4, 4);
  init_matrix(&current_product, 4, 4);

  int i;
  for(i = 0; i < num_particles; i++){
    Quaternion* current_attitude = &particles[i]->rot;
    
    outer_product(current_attitude, current_attitude, &current_product);
    m_add(&current_product, &average_product, &average_product);
  }

  double evector[4][4] = {
                           {1, 0, 0, 0},
                           {0, 1, 0, 0},
                           {0, 0, 1, 0},
                           {0, 0, 0, 1}
  };

  double prev[4];
  double min_variance = 1;
  
  int cur_vector;
  for(cur_vector = 0; cur_vector < 4; cur_vector++){
    double evalue;
    int steps = 50;
    while(steps > 0){
      steps--;

      int i;
      double norm = 0;
      int tmp[4] = {0, 0, 0, 0};
      for(i = 0; i < average_product.y_dimension; i++){
        int j;
        for(j = 0; j < average_product.x_dimension; j++){
          /* This is not matrix multiplication. i,j indexing instead of j,i for speed */
          tmp[i] += m_get(&average_product, i, j) * evector[cur_vector][j];
        }
        norm += tmp[i] * tmp[i];
      }

      /* Small epsilon to avoid division by zero */
      evalue = tmp[cur_vector]/(fabsf(evector[cur_vector][cur_vector]) + 0.000000001);
      evalue = fabsf(evalue / num_particles);

      for(i = 0; i<average_product.y_dimension; i++){
        prev[i] = evector[cur_vector][i];
        evector[cur_vector][i] = tmp[i] / sqrt(norm);
      }
    }
    double variance = 1 - (evalue - 0.25) / 0.75;
    if(variance < min_variance){
      min_variance = variance;
    }
  }

  if(min_variance < 0){
    min_variance = 0;
  }

  return min_variance;
}

double calculate_velocity_order(Simulator* sim){
  Vector3 average_velocity;

  int i;
  for(i = 0; i < sim->num_particles; i++){
    Vector3 current_velocity;
    current_velocity.x = 1 / (double)sim->num_particles;
    current_velocity.y = 0;
    current_velocity.z = 0;
    rotate(&sim->particles[i]->rot, &current_velocity);
    vec3_add(&average_velocity, &current_velocity, &average_velocity);
  }

  double length = vec3_dot(&average_velocity, &average_velocity);
  return sqrt(length);

}

/* Adds a particle and all it's neighbours to the cluster */
void add_to_cluster(Simulator* sim, int id, int cluster, int* particle_assignments){
  Particle* particle_buffer[150];

  /* The particle already belongs to a cluster */
  if(*(particle_assignments + id) != 0){
      return;
  }
  *(particle_assignments + id) = cluster;

  Particle* particle = &sim->particle_storage[id];

  int n = find_neighbors(
    &sim->grid,
    &particle->pos,
    sim->interaction_radius * 1.1,
    150,
    particle_buffer);

  int i;
  for(i = 0; i < n; i++){
    Particle* neighbor = particle_buffer[i];

    if(neighbor->id == id){
      continue;
    }

    int neighbor_cluster = particle_assignments[neighbor->id];
    
    if(neighbor_cluster != 0 && neighbor_cluster != cluster){
      printf("Particle already assigned to a wrong cluster.\n");
    }


    if(neighbor_cluster == 0 && dist(&particle->rot,&neighbor->rot) < 0.5){
      add_to_cluster(sim, neighbor->id, cluster, particle_assignments);
    }
  }

}

int count_distance_clusters(Simulator* sim){
  int particle_assignments[sim->num_particles];
  int i;

  for(i = 0; i< sim->num_particles; i++){
    particle_assignments[i] = 0;
  }

  int current_cluster = 1;
  for(i = 0; i< sim->num_particles; i++){
    if(particle_assignments[i] != 0){
      continue;
    }
    add_to_cluster(sim, i, current_cluster, particle_assignments);
    current_cluster++;
  }

  return current_cluster-1;

}

/* Returns the mean variance of the clusters */
double attitude_k_means(Simulator* sim, int k){
  Quaternion centroids[k]; 
  int i;
  for(i = 0; i < k; i++){
    centroids[i].q0 = 0;
    centroids[i].q1 = 0;
    centroids[i].q2 = 0;
    centroids[i].q3 = 0;
    random_rotation(&centroids[i]);
  }


  int epochs = 50;
  int cur_epoch;
  for(cur_epoch = 0; cur_epoch < epochs; cur_epoch++){
    Quaternion new_centroids[k];
    for(i = 0; i < k; i++){
      new_centroids[i] = centroids[i];
    }

    /* Assign each particle to a centroid */
    for(i = 0; i < sim->num_particles; i++){
      Particle* particle = &sim->particle_storage[i];

      int j;
      int assigned_centroid = 0;
      double min_dist = dist(&centroids[0], &particle->rot);
      for(j = 1; j<k; j++){
        double d = dist(&centroids[j], &particle->rot);
        if(d < min_dist){
          min_dist = d;
          assigned_centroid = j;
        }
      }
      if(dot(&particle->rot, &centroids[0]) < 0){
        negate(&particle->rot);
      }
      particle->cluster = assigned_centroid;

      new_centroids[assigned_centroid].q0 += particle->rot.q0;
      new_centroids[assigned_centroid].q1 += particle->rot.q1;
      new_centroids[assigned_centroid].q2 += particle->rot.q2;
      new_centroids[assigned_centroid].q3 += particle->rot.q3;
    }

    for(i = 0; i<k; i++){
      normalize(&new_centroids[i]);
      centroids[i] = new_centroids[i];
    }
  }
  
  double variance[k];
  int non_zero = 0;
  Particle* buffer[sim->num_particles];
  int current_cluster;
  double max = 0;
  for(current_cluster = 0; current_cluster< k; current_cluster++){
    variance[i] = 0;

    int j;
    int count = 0;
    for(j = 0; j < sim->num_particles; j++){
      Particle* particle = &sim->particle_storage[j];
      if(particle->cluster == current_cluster){
        buffer[count] = particle;
        count++;
      }
    }
    if(count != 0){
      non_zero++;
    }

    variance[current_cluster] = calculate_variance_raw(buffer, count);
    if(max < variance[current_cluster]){
      max = variance[current_cluster];
    }
  }

  return max;
}

double average_neighbors(Simulator* sim){
  Particle* buffer[150];
  
  int i;
  double average = 0;
  for(i = 0; i<sim->num_particles; i++){
    int n = find_neighbors(
      &sim->grid,
      &sim->particles[i]->pos,
      sim->interaction_radius,
      150,
      buffer);
    average = ((average * i) + n * n)/((double)(i+1));
  }

  return average;
}

int estimate_k(Simulator* sim){
  int k_min = 1;
  int k_max = 8;

  int k;
  for(k = k_min; k < k_max; k++){
    int epochs = 2;
    double min = 1;
    while(epochs > 0){
      epochs--;
      double var = attitude_k_means(sim, k);
      if(var < min){
        min = var;
      }
    }
    if(min < 0.1){
      return k;
    }
  }
  return k;
}

