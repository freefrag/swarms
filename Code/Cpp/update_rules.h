#ifndef UPDATE_RULES_H
#define UPDATE_RULES_H
#include "common.h"
#include "quaternion.h"
#include "simulator_common.h"

typedef struct {
  char* name;
  int neighbors;
  void (*update_attitude)(Particle*, Particle**, size_t);
} Update_Rule;

typedef struct {
  int count;
  Update_Rule *rules;
} Rules;


extern Rules all_rules;

#endif
