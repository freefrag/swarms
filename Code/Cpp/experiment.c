#include <stdio.h>
#include "experiment.h"
#include "simulator.h"
#include "statistics.h"

void init_experiment(Experiment* exp){
  exp->out = fopen("result.csv","w");
  exp->stage = 0;
  exp->var = 0;
  exp->count = 0;
}

void record_data(Simulator* sim, Experiment* exp){
  int warmup = 1000;
//  sim->noise = 0.075 - exp->stage * 0.0015;

  /* Experiment hasn't started yet, or has finished already */
  if(sim->frame < warmup){
    return;
  }

  if(sim->frame % 3000 >= 1000 && sim->frame % 100 == 0){
    exp->var += calculate_variance(sim);
    exp->count++;
  }

  if(sim->frame % 3000 == 2999){
    fprintf(exp->out, "%.4f, %.4f\n", sim->noise, exp->var / exp->count);
    fflush(exp->out);

    exp->stage++;
    exp->count = 0;
    exp->var = 0;
    if(exp->stage > 50) {
      /* Restart Experiment - Ugly Hack */
      exp->stage = 0;
      sim->frame = 0;
    }
  }

}
