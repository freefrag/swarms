#ifndef PARTICLE_MAN_H
#define PARTICLE_MAN_H
#include "simulator_common.h"

typedef struct {
  int num_elements;
  int capacity;
  Particle **particles; 
} Box;

typedef struct{
  /* The number of elements in the grid */
  int num_elements;
  /* The number of boxes in the grid */
  int num_boxes;
  /* The number of boxes in each row */
  int row_size;
  /* The length of each box */
  double box_length;
  Box *boxes;
} Grid;


/* Initializes the grid structure, with the given maximum number of particles */
void init_grid(Grid*, int);
/* Inserts a particle into the grid */
void insert_particle(Grid*, Particle*);
/* Removes the given particle from the Grid (Only works if it's position doesn't change after insertion */
void remove_particle(Grid*, Particle*);
/* Finds the requested number of particles around a point and places pointers to them in the provided buffer. Returns the number of particles added to the buffer */
int find_neighbors(Grid*, Vector3*,double, int, Particle**);
#endif
