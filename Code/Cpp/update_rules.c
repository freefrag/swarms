#include "quaternion.h"
#include "simulator_common.h"
#include "update_rules.h"

void average_attitude(Particle* particle, Particle** buffer, size_t num_particles);
void average_velocity(Particle* particle, Particle** buffer, size_t num_particles);
void constant_attitude(Particle* particle, Particle** buffer, size_t num_particles);
void copy_attitude(Particle* particle, Particle** buffer, size_t num_particles);
void nearest_frame(Particle* particle, Particle** buffer, size_t num_particles);
void nearest_opposite_frame(Particle* particle, Particle** buffer, size_t num_particles);

Update_Rule rules[] = 
{
  {
    .name = "Average Attitude",
    .neighbors = 100,
    .update_attitude = &average_attitude
  },
  {
    .name = "Constant Attitude",
    .neighbors = 0,
    .update_attitude = &constant_attitude
  },
  {
    .name = "Copy Attitude",
    .neighbors = 1,
    .update_attitude = &copy_attitude
  },
  {
    .name = "Nearest Frame",
    .neighbors = 1,
    .update_attitude= &nearest_frame
  },
  {
    .name = "Nearest Opposite Frame",
    .neighbors = 1,
    .update_attitude = &nearest_opposite_frame
  },
  {
    .name = "Average Velocity",
    .neighbors = 7,
    .update_attitude = &average_velocity
  },
};

Rules all_rules = 
{
  .count = sizeof(rules) / sizeof(Update_Rule),
  .rules = rules
};

void constant_attitude(Particle* particle, Particle** buffer, size_t num_particles){
  return;
}

void average_attitude(Particle* particle, Particle** buffer, size_t num_particles){
  double q0 = 0.0;
  double q1 = 0.0;
  double q2 = 0.0;
  double q3 = 0.0;

  int i;
  for (i = 0; i < num_particles; i++){
    if(dot(&particle->rot,&buffer[i]->rot) < 0){
      negate(&buffer[i]->rot);
    }
    q0 += buffer[i]->rot.q0;
    q1 += buffer[i]->rot.q1;
    q2 += buffer[i]->rot.q2;
    q3 += buffer[i]->rot.q3;
  }

  q0 += particle->rot.q0;
  q1 += particle->rot.q1;
  q2 += particle->rot.q2;
  q3 += particle->rot.q3;
  num_particles++;

  particle->rot.q0 = q0;
  particle->rot.q1 = q1;
  particle->rot.q2 = q2;
  particle->rot.q3 = q3;
  normalize(&particle->rot);
}

void average_velocity(Particle* particle, Particle** buffer, size_t num_particles){
  if(num_particles < 2){
    return;
  }

  Vector3 speed;
  speed.x = 1;
  speed.y = 0;
  speed.z = 0;

  Vector3 average_velocity = speed;
  rotate(&particle->rot, &average_velocity);
  int i;
  for(i = 0; i < num_particles; i++){
    Vector3 current_velocity = speed;
    rotate(&buffer[i]->rot, &current_velocity);
    vec3_add(&average_velocity, &current_velocity, &average_velocity);
  }
  

  from_vector_vector(&speed, &average_velocity, &particle->rot);
}


void copy_attitude(Particle* particle, Particle** buffer, size_t num_particles){
  if(num_particles == 0){
    return;
  }
  particle->rot.q0 = buffer[0]->rot.q0;
  particle->rot.q1 = buffer[0]->rot.q1;
  particle->rot.q2 = buffer[0]->rot.q2;
  particle->rot.q3 = buffer[0]->rot.q3;
}

void nearest_frame(Particle* particle, Particle** buffer, size_t num_particles){
  if(num_particles == 0){
    return;
  }

  /* approximately 1/sqrt(2) */
  double sqrt_half = 0.7071067811865475244008443621;
  Quaternion frames[] = { 
    {1,                  0,          0,          0},
    {0,                  1,          0,          0},
    {0,                  0,          1,          0},
    {0,                  0,          0,          1},
    {sqrt_half,  sqrt_half,          0,          0},
    {sqrt_half,          0,  sqrt_half,          0},
    {sqrt_half,          0,          0, sqrt_half},
    {sqrt_half, -sqrt_half,          0,          0},
    {sqrt_half,          0, -sqrt_half,          0},
    {sqrt_half,          0,          0, -sqrt_half} 
  };

  double min_dist = 9999999;
  Quaternion *min_quaternion;
  int i;
  int length = sizeof(frames) / sizeof(Quaternion);
  for(i = 0; i < length; i++){
    multiply(frames + i, &buffer[0]->rot, frames + i);
    double distance = dist(frames + i, &particle->rot);
    if(distance < min_dist){
      min_dist = distance;
      min_quaternion = frames + i;
    }
  }

  particle->rot.q0 = min_quaternion->q0;
  particle->rot.q1 = min_quaternion->q1;
  particle->rot.q2 = min_quaternion->q2;
  particle->rot.q3 = min_quaternion->q3;
  normalize(&particle->rot);
}

void nearest_opposite_frame(Particle* particle, Particle** buffer, size_t num_particles){
  if(num_particles == 0){
    return;
  }

  /* approximately 1/sqrt(2) */
  double sqrt_half = 0.7071067811865475244008443621;
  Quaternion frames[] = { 
    {1,                  0,          0,          0},
    {0,                  1,          0,          0},
    {0,                  0,          1,          0},
    {0,                  0,          0,          1},
    /*{sqrt_half,  sqrt_half,          0,          0},
    {sqrt_half,          0,  sqrt_half,          0},
    {sqrt_half,          0,          0, sqrt_half},
    {sqrt_half, -sqrt_half,          0,          0},
    {sqrt_half,          0, -sqrt_half,          0},
    {sqrt_half,          0,          0, -sqrt_half} */
  };

  double min_dist = 9999999;
  Quaternion *min_quaternion;
  int i;
  int length = sizeof(frames) / sizeof(Quaternion);
  for(i = 0; i < length; i++){
    multiply(frames + i, &buffer[0]->rot, frames + i);
    double distance = dist(frames + i, &particle->rot);
    if(distance < min_dist){
      min_dist = distance;
      min_quaternion = frames + i;
    }
  }

  particle->rot.q0 = min_quaternion->q0;
  particle->rot.q1 = min_quaternion->q1;
  particle->rot.q2 = min_quaternion->q2;
  particle->rot.q3 = min_quaternion->q3;
  normalize(&particle->rot);
}
