#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "particle_man.h"
#define BOX_SIZE_MULTIPLIER 1.2
#define INITIAL_BOX_CAPACITY 8
#define OVERFLOW_MAGIC 2
#define UNDERFLOW_MAGIC 0.5


void init_box(Box *box){
  box->num_elements = 0;
  box->capacity     = INITIAL_BOX_CAPACITY;
  box->particles    = calloc(INITIAL_BOX_CAPACITY, sizeof(Particle*));
}

/* Initiates the grid and allocated memory for all the particles */
void init_grid(Grid *grid, int num_particles) {

  double particle_separation = 1 / cbrt((double) num_particles);
  double box_length = BOX_SIZE_MULTIPLIER * particle_separation;
  int row_size = ceil(1 / (box_length));
  int num_boxes = row_size * row_size * row_size;

  Box *boxes = malloc(num_boxes * sizeof(Box));
  if (boxes == NULL) {
    fputs("Unable to allocate memory for the grid", stderr);
    exit(-1);
  }
  
  int i;
  for(i = 0; i<num_boxes; i++){
    init_box(boxes + i);
  }

  grid->num_elements = 0;
  grid->num_boxes    = num_boxes;
  grid->row_size     = row_size;
  grid->box_length   = box_length;
  grid->boxes        = boxes;

}

int get_box_index(Grid *grid, double position) {
  return floor(position / grid->box_length);
}

/* Returns the index of the box a particle at this position would belong to */
int get_safe_box_index(Grid *grid, double position) {
  return pos_mod(get_box_index(grid, position), grid->row_size);
}

void handle_overflow(Box *box){
  int old_capacity = box->capacity;
  int new_capacity = box->capacity*OVERFLOW_MAGIC;

  /* realloc can return NULL, need to handle that case :( */
  box->capacity = new_capacity;
  box->particles = realloc_zero(box->particles,
                                old_capacity * sizeof(Particle*),
                                new_capacity  * sizeof(Particle*));
}

int get_particle_index(Box* box, Particle* particle){
  Particle** cur_particle = box->particles;

  int i;
  for(i = 0; i<box->capacity; i++){
    if(*cur_particle){
    }
    if ( (*cur_particle) == particle){
      return i;
    }
    cur_particle++;
  }

  return -1;
}

void insert_particle(Grid *grid, Particle* particle){
  int box_i = get_safe_box_index(grid, particle->pos.x);
  int box_j = get_safe_box_index(grid, particle->pos.y);
  int box_k = get_safe_box_index(grid, particle->pos.z);

  //printf("Adding particle to box:{%d,%d,%d}\n", box_i, box_j, box_k);
  int row_size = grid->row_size;
  Box *box = &grid->boxes[box_i * row_size * row_size + box_j * row_size + box_k];

  if(box->num_elements >= box->capacity){
    handle_overflow(box);
  }

  //int free_index = get_particle_index(box, NULL);
  box->particles[box->num_elements] = particle;
  box->num_elements++;
  grid->num_elements++;
}

void handle_underflow(Box *box){
  int old_capacity = box->capacity;
  int new_capacity = ceil(box->capacity * UNDERFLOW_MAGIC);
  if (new_capacity < INITIAL_BOX_CAPACITY || new_capacity < box->num_elements){
    return;
  }

  box->capacity  = new_capacity;
  box->particles = realloc_zero(box->particles,
                                old_capacity * sizeof(Particle*),
                                new_capacity  * sizeof(Particle*));
}

void remove_particle(Grid *grid, Particle* particle){
  int box_i = get_safe_box_index(grid, particle->pos.x);
  int box_j = get_safe_box_index(grid, particle->pos.y);
  int box_k = get_safe_box_index(grid, particle->pos.z);
  
  int row_size = grid->row_size;
  Box *box = &grid->boxes[box_i * row_size * row_size + box_j * row_size + box_k];


  int particle_index = get_particle_index(box, particle);
  if(particle_index != -1){
    grid->num_elements--;
    box->num_elements--;
    box->particles[particle_index] = box->particles[box->num_elements];
    box->particles[box->num_elements] = NULL;
  }

  if(box->num_elements <= box->capacity * (UNDERFLOW_MAGIC-0.1)){
    handle_underflow(box);
  }
}

double dist_squared(Vector3* p1, Vector3* p2){
  double d_x = p1->x - p2->x;
  double d_y = p1->y - p2->y;
  double d_z = p1->z - p2->z;
  return d_x * d_x + d_y * d_y + d_z * d_z;
}

static Particle* part_buffer[20000];
int find_neighbors(Grid* grid, Vector3* position, double radius,
                    int search_number, Particle** buffer){
  if (search_number == 0){
    return 0;
  }

  int box_i_start = get_box_index(grid, position->x - radius);
  int box_i_end = get_box_index(grid, position->x + radius);
  int box_j_start = get_box_index(grid, position->y - radius);
  int box_j_end = get_box_index(grid, position->y + radius);
  int box_k_start = get_box_index(grid, position->z - radius);
  int box_k_end = get_box_index(grid, position->z + radius);

  
  int i;
  int num_neighbors = 0;
  double radius_squared = radius*radius;
  int row_size = grid->row_size;
  for (i = box_i_start; i <= box_i_end; i++){
    int box_i = pos_mod(i, grid->row_size);
    int j;
    for (j = box_j_start; j <= box_j_end; j++){
      int box_j = pos_mod(j, grid->row_size);
      int k;
      for (k = box_k_start; k <= box_k_end; k++){
        int box_k = pos_mod(k, grid->row_size);
        Box *box = &grid->boxes[box_i * row_size * row_size + box_j * row_size + box_k];
        int h;
        for(h = 0; h< box->capacity; h++){
          if(!box->particles[h]){
            continue;
          }
          if (dist_squared(&box->particles[h]->pos, position) < radius_squared){
            part_buffer[num_neighbors] = box->particles[h];
            num_neighbors++;
          }
        }
      }
    }
  }

  if (num_neighbors < search_number){
    search_number = num_neighbors;
  }

  for(i = 0; i < search_number; i++){
    int cur = rand_range(0, num_neighbors);
    buffer[i] = part_buffer[cur];

    part_buffer[cur] = part_buffer[num_neighbors-1];
    part_buffer[num_neighbors-1] = NULL;
    num_neighbors--;
  }
  
  return search_number;

}
