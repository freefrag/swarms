#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "simulator_common.h"
#include "common.h"

int pos_mod(int i, int n){
  return (i%n + n) % n;
}

double pos_fmod(double i, double n){
  return fmod(fmod(i,n) + n, n);
}

/* Resizes the buffer and sets the new memory to 0 */
void* realloc_zero(void* ptr_buffer, size_t old_size, size_t new_size){
  void* ptr_new = realloc(ptr_buffer, new_size);
  if (ptr_new == NULL){
    free(ptr_buffer);
    return NULL;
  }
  if (new_size > old_size){
    size_t diff = new_size - old_size;
    void* ptr_start = ((char*)ptr_new) + diff;
    memset(ptr_start, 0, diff);
  }
  return ptr_new;
}

double rand_double(void){
  return rand() / (double)RAND_MAX;
}

/* Returns a 'uniformly sampled' integer between Low (inclusive) and High (exclusive) */
int rand_range(int low, int high){
  int diff = high - low;
  return low + floor(rand_double() * diff);
}

void rand_position(Vector3* vec){
  vec->x = rand_double();
  vec->y = rand_double();
  vec->z = rand_double();
}

double vec3_dist(Vector3* v1, Vector3* v2){
  double dx = v1->x - v2->x;
  double dy = v1->y - v2->y;
  double dz = v1->z - v2->z;

  return dx * dx + dy * dy + dz * dz;
}

double vec3_dot(Vector3* v1, Vector3* v2){
  return v1->x*v2->x + v1->y*v2->y + v1->z*v2->z;
}

void vec3_cross(Vector3* v1, Vector3* v2, Vector3* out){
  Vector3 result;
  result.x = v1->y*v2->z - v1->z*v2->y;
  result.y = v1->z*v2->x - v1->x*v2->z;
  result.z = v1->x*v2->y - v1->y*v2->x;

  out->x = result.x;
  out->y = result.y;
  out->z = result.z;
}

void vec3_normalize(Vector3* in, Vector3* out){
  double length = vec3_dot(in, in);
  length = sqrt(length);

  out->x = in->x / length;
  out->y = in->y / length;
  out->z = in->z / length;
}

void vec3_add(Vector3* v1, Vector3* v2, Vector3* out){
  out->x = v1->x + v2->x;
  out->y = v1->y + v2->y;
  out->z = v1->z + v2->z;
}
