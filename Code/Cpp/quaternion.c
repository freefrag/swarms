#define _USE_MATH_DEFINES
#include <math.h>
#include <stdio.h>
#include "quaternion.h"

/* Multiplies a and b, storing the result in res */
void multiply(Quaternion* a, Quaternion* b, Quaternion* res){
  double q0 = a->q0 * b->q0 - a->q1 * b->q1 - a->q2 * b->q2 - a->q3 * b->q3;
  double q1 = a->q0 * b->q1 + a->q1 * b->q0 + a->q2 * b->q3 - a->q3 * b->q2;
  double q2 = a->q0 * b->q2 - a->q1 * b->q3 + a->q2 * b->q0 + a->q3 * b->q1;
  double q3 = a->q0 * b->q3 + a->q1 * b->q2 - a->q2 * b->q1 + a->q3 * b->q0;

  res->q0 = q0;
  res->q1 = q1;
  res->q2 = q2;
  res->q3 = q3;
}

void toAxisAngle(Quaternion* q, double* angle, double* x, double* y, double* z){
  if(q->q0 > 1){
    normalize(q);
  }

  double q0s = q->q0 * q->q0;
  *angle = 2 * acos(q->q0);
  
  if (angle == 0){
    return;
  }

  *x = q->q1 / sqrt(1-q0s);
  *y = q->q2 / sqrt(1-q0s);
  *z = q->q3 / sqrt(1-q0s);
}

void toEuler(Quaternion* q, double* x, double* y, double* z){
  float sqw = q->q0 * q->q0;
  float sqx = q->q1 * q->q1;
  float sqy = q->q2 * q->q2;
  float sqz = q->q3 * q->q3;

  *x = (float)atan2(2.0 * ( q->q2 * q->q3 + q->q1 * q->q0 ) , ( -sqx - sqy + sqz + sqw ));
  *y = (float)asin(-2.0 * ( q->q1 * q->q3 - q->q2 * q->q0 ));
  *z = (float)atan2(2.0 * ( q->q1 * q->q2 + q->q3 * q->q0 ) , (  sqx - sqy - sqz + sqw ));
}

/* Returns the distance between quaternions, roughly 1/2(1-cos(x)) */
double dist(Quaternion* a, Quaternion* b){
  double inner = dot(a,b);
  return 1 - inner * inner;
}

double dot(Quaternion* a, Quaternion* b){
  return a->q0 * b->q0 + a->q1 * b->q1 + a->q2 * b->q2 + a->q3 * b->q3;
}

void negate(Quaternion* q){
  q->q0 = -q->q0;
  q->q1 = -q->q1;
  q->q2 = -q->q2;
  q->q3 = -q->q3;
}

/* Conjugates the quaternion */
void conjugate(Quaternion* q){
  q->q1 = -q->q1;
  q->q2 = -q->q2;
  q->q3 = -q->q3;
}

/* Inverts the quaternion */
void inverse(Quaternion* q){
  conjugate(q);
  double len = norm_squared(q);
  q->q0 /= len;
  q->q1 /= len;
  q->q2 /= len;
  q->q3 /= len;
}

/* Returns the squared norm of the quaternion */
double norm_squared(Quaternion* q){
  return q->q0 * q->q0 + q->q1 * q->q1 + q->q2 * q->q2 + q->q3 * q->q3;
}

/* Returns the norm of the quaternion */
double norm(Quaternion* q1){
  return sqrt(norm_squared(q1));
}

/* Normalizes the given quaternion */
void normalize(Quaternion* q){
  double len = norm(q);
  q->q0 /= len;
  q->q1 /= len;
  q->q2 /= len;
  q->q3 /= len;
}

/* Overwrites q with a random rotation vector */
void random_rotation(Quaternion* q){
  double x0 = rand_double();
  double x1 = rand_double() * 2 * M_PI;
  double x2 = rand_double() * 2 * M_PI;

  double s1 = sin(x1);
  double c1 = cos(x1);
  double s2 = sin(x2);
  double c2 = cos(x2);

  double r1 = sqrt(1 - x0);
  double r2 = sqrt(x0);

  q->q0 = s1 * r1;
  q->q1 = c1 * r1;
  q->q2 = s2 * r2;
  q->q3 = c2 * r2;
}

void outer_product(Quaternion* a, Quaternion* b, Matrix* dest){
  if(dest->x_dimension != 4 && dest->y_dimension != 4){
    printf("Warning, mismatched matrix dimensions for outer product");
    return;
  }
  
  m_set(dest, 0,0, a->q0 * b->q0);
  m_set(dest, 0,1, a->q0 * b->q1);
  m_set(dest, 0,2, a->q0 * b->q2);
  m_set(dest, 0,3, a->q0 * b->q3);
  m_set(dest, 1,0, a->q1 * b->q0);
  m_set(dest, 1,1, a->q1 * b->q1);
  m_set(dest, 1,2, a->q1 * b->q2);
  m_set(dest, 1,3, a->q1 * b->q3);
  m_set(dest, 2,0, a->q2 * b->q0);
  m_set(dest, 2,1, a->q2 * b->q1);
  m_set(dest, 2,2, a->q2 * b->q2);
  m_set(dest, 2,3, a->q2 * b->q3);
  m_set(dest, 3,0, a->q3 * b->q0);
  m_set(dest, 3,1, a->q3 * b->q1);
  m_set(dest, 3,2, a->q3 * b->q2);
  m_set(dest, 3,3, a->q3 * b->q3);
}

void rotate(Quaternion* q, Vector3* d){
  Quaternion temp;
  temp.q0 = 0;
  temp.q1 = d->x;
  temp.q2 = d->y;
  temp.q3 = d->z;
  Quaternion qi = *q;
  inverse(&qi);

  multiply(q, &temp, &temp);
  multiply(&temp, &qi, &temp);
  d->x = temp.q1;
  d->y = temp.q2;
  d->z = temp.q3;
}

void from_vector_vector(Vector3* v1, Vector3* v2, Quaternion* out){
  vec3_normalize(v1, v1);
  vec3_normalize(v2, v2);

  Vector3 cross;
  double cos = vec3_dot(v1, v2);
  double half_cos = sqrt(0.5 * (1 + cos));
  vec3_cross(v1, v2, &cross);

  out->q0 = half_cos;
  out->q1 = cross.x / (2 * half_cos);
  out->q2 = cross.y / (2 * half_cos);
  out->q3 = cross.z / (2 * half_cos);
}
