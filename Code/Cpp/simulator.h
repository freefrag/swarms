#ifndef SIMULATOR_H 
#define SIMULATOR_H
#include "particle_man.h"
#include "update_rules.h"


typedef struct {
  long frame;
  size_t num_particles;
  double interaction_radius;
  double noise;
  Grid grid;
  Update_Rule *rule;
  Particle **particles;      
  Particle *particle_storage;
} Simulator;


void init_simulator(Simulator*, size_t, double, double,  Update_Rule*);
void step_simulator(Simulator*);
#endif
