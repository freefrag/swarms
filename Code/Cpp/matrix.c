#include <math.h>
#include <stdio.h>
#include "matrix.h"

void m_add(Matrix* a, Matrix* b, Matrix* dest){
  if(a->x_dimension != b->x_dimension || a->y_dimension != b->y_dimension ||
     a->x_dimension != dest->x_dimension || a->y_dimension != dest->y_dimension)
  {
    printf("Warning, mismatched matrix dimensions");
  }
  
  int i;
  int j;
  for(i = 0; i < a->y_dimension; i++){
    for(j = 0; j< a->x_dimension; j++){
      m_set(dest, i,j, m_get(a,i,j) + m_get(b,i,j));
    }
  }
}

void init_matrix(Matrix* matrix, int x_dim, int y_dim){
  matrix->x_dimension = x_dim;
  matrix->y_dimension = y_dim;
  matrix->data = calloc(x_dim * y_dim, sizeof(double));
}

int calculate_index(Matrix* matrix, int i, int j){
  if(i >= matrix->y_dimension || j >= matrix->x_dimension){
    printf("Warning, getting out of bounds index");
  }

  return i * matrix->y_dimension + j;
}

void m_set(Matrix* matrix, int i, int j, double value){
  matrix->data[calculate_index(matrix, i, j)] = value;
}

double m_get(Matrix* matrix, int i, int j){
  return matrix->data[calculate_index(matrix, i, j)];
}

void destroy_matrix(Matrix* matrix){
  matrix->x_dimension = -1;
  matrix->y_dimension = -1;
  if(matrix->data != NULL){
    free(matrix->data);
  }
  matrix->data = NULL;
}

