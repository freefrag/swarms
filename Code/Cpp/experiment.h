#ifndef EXPERIMENT_H
#define EXPERIMENT_H
#include <stdio.h>
#include "simulator.h"

typedef struct {
  FILE* out;
  int stage;
  double var;
  int count;
} Experiment;

void init_experiment(Experiment*);
void record_data(Simulator*, Experiment*);
#endif
